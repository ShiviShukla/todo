let inputButton = document.querySelector(".home-container .main-section .form .todo");
let list = document.querySelector(".home-container .main-section .todo-list");
let todoItems = []

function addTodoItem(event) {
    if (event.keyCode === 13) {
        event.preventDefault();
        todoItems.push({ todoText: inputButton.value, completed: false });
        inputButton.value = '';
        domRefresh();
    }
};

function createList() {
    for (index in todoItems) {
        const li = document.createElement('li');
        const item = document.createElement('div');
        const del = document.createElement('button');
        const checkButton = document.createElement('input');

        checkButton.setAttribute('class', 'checkbox-round');
        checkButton.setAttribute('type', 'checkbox');
        checkButton.setAttribute('onclick', 'checkBox(event)');

        item.textContent = todoItems[index]["todoText"];
        item.setAttribute('class', 'item-value');

        li.setAttribute('class', 'list-items');

        del.setAttribute('onclick', 'deleteItem(event)');
        del.setAttribute('font-size', '100px');
        del.innerHTML = '&#xf00d;'
        del.className = 'delete fa';

        if (todoItems[index]['completed'] == true) {
            li.style.textDecoration = 'line-through';
        };
        li.append(checkButton);
        li.append(item);
        li.append(del);
        list.append(li);
    }
};


function domRefresh() {
    while (list.firstChild) {
        list.removeChild(list.firstChild)
    };
    createList();
};

function checkBox(e) {
    var liValue = null;
    if (e.target.className == "checkbox-round") {
        liItem = (e.target.parentElement)
        liValue = liItem.querySelector('.list-items .item-value').textContent
        for (index in todoItems) {
            if (todoItems[index]['todoText'] == liValue && todoItems[index]['completed'] == false) {
                todoItems[index]['completed'] = true;
            }
            else if (todoItems[index]['todoText'] == liValue && todoItems[index]['completed'] == true) {
                todoItems[index]['completed'] = false;
            }
        } domRefresh();
    };
}

function deleteItem(event) {
    liItem = event.target.parentElement;
    liValue = liItem.querySelector('.list-items .item-value').textContent
    console.log(liValue)

    for (index in todoItems) {
        if (todoItems[index]['todoText'] == liValue) {
            delete todoItems[index];
            domRefresh();
        }
    }
}
// checkBox
// d = document.querySelectorAll('.home-container .main-section .todo-list .list-items .delete');


// list.addEventListener('click', function (e) {
//     var liValue = null;
//     if (e.target.className == "delete fa") {
//         liItem = (e.target.parentElement);
//         liValue = liItem.querySelector('.list-items .item-value').textContent
//         for (index in todoItems) {
//             if (todoItems[index]['todoText'] == liValue) {
//                 delete todoItems[index];
//                 domRefresh();
//             }
//         }
//     }
// }
// );

// console.log(d)
// d.addEventListener('keypress', function (e) {
    // console.log(e.target.parentElement)
// })


// list.addEventListener('click', function (e) {
//     var liValue = null;
//     if (e.target.className == "delete fa") {
//         liItem = (e.target.parentElement);
//         liValue = liItem.querySelector('.list-items .item-value').textContent
//         for (index in todoItems) {
//             if (todoItems[index]['todoText'] == liValue) {
//                 delete todoItems[index];
//                 domRefresh();
//             }
//         }
//     }
// }
// );







// var inputButton = document.querySelector(".home-container .main-section .form .todo");
// var list = document.querySelector(".home-container .main-section .todo-list ");

// inputButton.addEventListener('keypress', function (e) {
//     if (e.keyCode == 13) {
//         e.preventDefault();
//         let list = document.querySelector(".home-container .main-section .todo-list");
//         const li = document.createElement('li');
//         const item = document.createElement('div');
//         const del = document.createElement('i');
//         const checkButton = document.createElement('input');

//         checkButton.setAttribute('class', 'checkbox-round');
//         checkButton.setAttribute('type', 'checkbox');

//         item.textContent = inputButton.value;
//         item.setAttribute('class', 'item-value');

//         li.setAttribute('class', 'list-items');
//         li.setAttribute('draggable', 'true');
//         // 
//         li.setAttribute('ondragend', 'dragEnd()');
//         li.setAttribute('ondragover', 'dragOver(event)');
//         li.setAttribute('ondragstart', 'dragStart(event)');
//         // 

//         del.setAttribute('font-size', '100px');
//         del.innerHTML = '&#xf00d;'
//         del.className = 'delete fa';

//         li.append(checkButton);
//         li.append(item);
//         li.append(del);
//         list.append(li)
//         inputButton.value = '';
//     };
// });

// list.addEventListener('click', function (e) {
//     if (e.target.className == "delete fa") {
//         var del = e.target.parentElement;
//         list.removeChild(del);
//     };
// });


// let footer = document.querySelector('.home-container .footer');
// footer.addEventListener('click', function (e) {
//     var ullist = document.querySelector(".home-container .main-section .todo-list");
//     var itemList = document.querySelectorAll(".home-container .main-section .todo-list .list-items");

//     if (e.target.className == 'all') {
//         itemList.forEach(function (li) {
//             li.style.display = 'flex';
//         })
//     }
//     if (e.target.className == 'complete') {
//         itemList.forEach(function (li) {
//             checkBtn = li.querySelector('.checkbox-round');
//             if (checkBtn.checked == false) {
//                 li.style.display = 'none';
//             }
//             else{
//                 li.style.display = 'flex';
//             }
//         })
//     }
//     if (e.target.className == 'active') {
//         itemList.forEach(function (li) {
//             checkBtn = li.querySelector('.checkbox-round');
//             if (checkBtn.checked == true) {
//                 li.style.display = 'none';
//             }
//             else{
//                 li.style.display = 'flex';
//             }
//         })
//     }
// })

// var selected
// function dragOver( e ) {
//  try{
//     if ( isBefore( selected, e.target ) ) e.target.parentNode.insertBefore( selected, e.target )
//     else e.target.parentNode.insertBefore( selected, e.target.nextSibling )
//  }catch(e){}
// }

// function dragEnd() {
//   selected = null
// }

// function dragStart( e ) {
//   e.dataTransfer.effectAllowed = "move"
//   e.dataTransfer.setData( "text/plain", null )
//   selected = e.target
// }

// function isBefore( el1, el2 ) {
//   var cur
//   if ( el2.parentNode === el1.parentNode ) {
//     for ( cur = el1.previousSibling; cur; cur = cur.previousSibling ) {
//       if (cur === el2) return true
//     }
//   } else return false;
// }
